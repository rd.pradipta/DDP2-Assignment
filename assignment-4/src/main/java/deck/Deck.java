/** Import required modules
* @author Rd Pradipta Gitaya S
* Class     : DDP2 - E
* NPM       : 1706043361
*/

package deck;

import javax.swing.Timer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;

import java.awt.Font;
import java.awt.Panel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.GridBagConstraints;
import java.awt.event.ActionListener;

public class Deck {

    private JLabel label;
    private JLabel welcomeMsg;
    private int tryCounter = 0;
    private int matchCounter = 0;
    private JButton card1 = null;
    private JButton card2 = null;
    private JPanel deckPanel = new JPanel();
    private JPanel otherPanel = new JPanel();
    private JFrame board = new JFrame("Judi Offline");
    private ArrayList<ImageIcon> cardContent = new ArrayList<>();
    private ArrayList<JButton> cardList = new ArrayList<>();
    
    GridBagConstraints layoutOptimize = new GridBagConstraints();
    
    /**
     * Constructor for Deck Class.
     * @return void, Will just add a constructor into Deck() class
     */

    public Deck() {
        board.setLayout(new GridBagLayout());
        deckMaker();
        stuffMaker();

        layoutOptimize.gridx = 0;
        layoutOptimize.gridy = 0;
        layoutOptimize.fill = GridBagConstraints.BOTH;
        
        board.add(deckPanel, layoutOptimize);
        layoutOptimize.gridy = 1;
        
        board.add(otherPanel, layoutOptimize);
        board.setVisible(true);
        board.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        board.pack();
    }

    /**
     * deckMaker() method will make grid 6x6 to store cards and their icons
     * @return void, Just modify instance variable's values 
     */

    private void deckMaker() {
        deckPanel.setLayout(new GridLayout(6, 6));

        for (int i = 0; i < 18; i++) {
            ImageIcon icon = new ImageIcon("img/anim" + i + ".png");
            cardContent.add(icon);
            cardContent.add(icon);
        }
        Collections.shuffle(cardContent);
        
        for (int i = 0; i < 36; i++) {
            JButton button = new JButton(new ImageIcon("img/depan.png"));
            cardList.add(button);
            button.setDisabledIcon(cardContent.get(i));
            button.addActionListener(e -> pressButton(button));
            deckPanel.add(button);
        }
    }

    /**
     * stuffMaker makes other than cards, Example: Exit button, Restart, etc.
     * @return void, Just modify instance variable's values 
     */

    public void stuffMaker() {

        otherPanel.setLayout(new GridBagLayout());

        JButton resetBt = new JButton("Reset");
        resetBt.addActionListener(e -> reset());
        layoutOptimize.gridx = 0;
        layoutOptimize.gridy = 0;
        otherPanel.add(resetBt, layoutOptimize);

        JButton exitBt = new JButton("Keluar");
        exitBt.addActionListener(e -> System.exit(0));
        layoutOptimize.gridx = 1;
        otherPanel.add(exitBt, layoutOptimize);

        label = new JLabel("Percobaan: " + tryCounter);
        layoutOptimize.gridy = 1;
        otherPanel.add(label, layoutOptimize);
    }

    /**
     * pressButton() method will be a checker if card already selected or not
     * @param JButton, an object of pressed button
     * @return void, Just modify instance variable's values 
     */

    public void pressButton(JButton btPressed) {
        btPressed.setEnabled(false);
        if (card1 == null && card2 == null) {
            card1 = btPressed;
        } else if (card1 != null && card2 == null) {
            card2 = btPressed;
            tryCounter++;
            label.setText("Percobaan: " + tryCounter);
            imageChecker();
            checkWin();
        } else if (card1 != null && card2 != null) {
            card1.setEnabled(true);
            card2.setEnabled(true);
            card1 = null;
            card2 = null;
            card1 = btPressed;
        }
    }

    /**
     * checkWin() method will check if all cards already matched or not, IF Yes, then will win the game
     * @return void, Just modify instance variable's values 
     */

    public void checkWin() {
        if (matchCounter == 36) {
            JOptionPane.showMessageDialog(board, "Game Over! You LOSE!");
        }
    }

    public void imageChecker() {
        if (card1.getDisabledIcon().equals(card2.getDisabledIcon())) {
            card2.setEnabled(false);
            card1.setVisible(false);
            card2.setVisible(false);
            matchCounter += 2;
            card1 = null;
            card2 = null;
        }
    }

    /**
     * Method to reset the game everytime user wish
     * @return void, Just modify instance variable's values 
     */
     
    public void reset() {
        for (JButton i: cardList) {
            i.setVisible(true);
            i.setEnabled(true);
        }
        matchCounter = 0;
        tryCounter = 0;
        label.setText("Percobaan: " + tryCounter);
    }

}