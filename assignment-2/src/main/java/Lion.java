import java.util.*;

public class Lion{

  private ArrayList<String> lion_name;

  public Lion(ArrayList<String> lion_name){
    this.lion_name = lion_name;
  }

  public boolean findLion(String lion_name){
    int counter = 0;
    for (String i : this.lion_name){
      if (i.equals(lion_name)) counter+=1;
      else{
        continue;
      }
    }
    if (counter == 0) return false;
    else{
      return true;
    }
  }

  public void lionMenu(String lion_name){
    System.out.println("You are visiting "+ lion_name + " (lion) now, what would you like to do?\n"+
    "1: See it hunting 2: Brush the mane 3: Disturb it");
  }

  public void doHunt(String lion_name){
    System.out.println("Lion is hunting..");
    System.out.println(lion_name +" makes a voice: err...!\n"+
                       "Back to the office!");
  }

  public void doBrush(String lion_name){
    System.out.println("Clean the lion's mane..");
    System.out.println(lion_name +" makes a voice: Hauhhmm!\n"+
                       "Back to the office!");
  }

  public void doDisturb(String lion_name){
    System.out.println(lion_name +" makes a voice: HAUHHMM!\n"+
                       "Back to the office!");
  }
}