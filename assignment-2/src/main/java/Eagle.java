import java.util.*;

public class Eagle{

  private ArrayList<String> eagle_name;

  public Eagle(ArrayList<String> eagle_name){
    this.eagle_name = eagle_name;
  }

  public boolean findEagle(String eagle_name){
    int counter = 0;
    for (String i : this.eagle_name){
      if (i.equals(eagle_name)) counter+=1;
      else{
        continue;
      }
    }
    if (counter == 0) return false;
    else{
      return true;
    }
  }

  public void eagleMenu(String eagle_name){
    System.out.println("You are visiting "+ eagle_name + " (eagle) now, what would you like to do?\n"+
    "1: Order to fly");
  }

  public void doFly(String eagle_name){
    System.out.println(eagle_name +" makes a voice: kwaakk...\n"+
                       "You hurt!\n"+
                       "Back to the office!");
  }
}