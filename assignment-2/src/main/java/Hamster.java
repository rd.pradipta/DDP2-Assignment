import java.util.*;

public class Hamster{

  private ArrayList<String> hamster_name;

  public Hamster(ArrayList<String> hamster_name){
    this.hamster_name = hamster_name;
  }

  public boolean findHamster(String hamster_name){
    int counter = 0;
    for (String i : this.hamster_name){
      if (i.equals(hamster_name)) counter+=1;
      else{
        continue;
      }
    }
    if (counter == 0) return false;
    else{
      return true;
    }
  }

  public void hamsterMenu(String hamster_name){
    System.out.println("You are visiting "+ hamster_name + " (hamster) now, what would you like to do?\n"+
    "1: See it gnawing 2: Order to run in the hamster wheel");
  }

  public void doGnawing(String hamster_name){
    System.out.println(hamster_name +" makes a voice: ngkkrit.. ngkkrrriiit\n"+
                       "Back to the office!");
  }

  public void doRun(String hamster_name){
    System.out.println(hamster_name +" makes a voice: trrr.... trrr....\n"+
                       "Back to the office!");
  }
}