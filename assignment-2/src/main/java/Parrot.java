import java.util.*;

public class Parrot{

  private ArrayList<String> parrot_name;

  public Parrot(ArrayList<String> parrot_name){
    this.parrot_name = parrot_name;
  }

  public boolean findParrot(String parrot_name){
    int counter = 0;
    for (String i : this.parrot_name){
      if (i.equals(parrot_name)) counter+=1;
      else{
        continue;
      }
    }
    if (counter == 0) return false;
    else{
      return true;
    }
  }

  public void parrotMenu(String parrot_name){
    System.out.println("You are visiting "+ parrot_name + " (parrot) now, what would you like to do?\n"+
    "1: Order to fly 2: Do conversation");
  }

  public void doFly(String parrot_name){
    System.out.println("Parrot "+parrot_name+" flies!");
    System.out.println(parrot_name +" makes a voice: FLYYYY.....\n"+
                       "Back to the office!");
  }

  public void doConversation(String parrot_name, String dialogue){
    System.out.println(parrot_name + " says: " + dialogue.toUpperCase());
    System.out.println("Back to the office!");
  }

  public void doNothing(String parrot_name){
    System.out.println(parrot_name + " says: HM?");
    System.out.println("Back to the office!");
  }
}