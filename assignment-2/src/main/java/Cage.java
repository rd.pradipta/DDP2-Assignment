import java.util.*;

public class Cage {

  private ArrayList<String> cat_name;
  private ArrayList<Integer> cat_length;

  private ArrayList<String> hamster_name;
  private ArrayList<Integer> hamster_length;
  
  private ArrayList<String> parrot_name;
  private ArrayList<Integer> parrot_length;

  private ArrayList<String> eagle_name;
  private ArrayList<Integer> eagle_length;

  private ArrayList<String> lion_name;
  private ArrayList<Integer> lion_length;

  public Cage(ArrayList<String> cat_name, ArrayList<String> hamster_name, ArrayList<String> parrot_name, ArrayList<String> eagle_name, ArrayList<String> lion_name,
              ArrayList<Integer> cat_length, ArrayList<Integer> hamster_length, ArrayList<Integer> parrot_length, ArrayList<Integer> eagle_length, ArrayList<Integer> lion_length){
    this.cat_name = cat_name;
    this.cat_length = cat_length;

    this.hamster_name = hamster_name;
    this.hamster_length = hamster_length;

    this.parrot_name = parrot_name;
    this.parrot_length = parrot_length;

    this.eagle_name = eagle_name;
    this.eagle_length = eagle_length;

    this.lion_name = lion_name;
    this.lion_length = lion_length;
  }
  public void catArrange(){
    ArrayList<String> cage = new ArrayList<String>();
    ArrayList<String> unsort = new ArrayList<String>();

    ArrayList<String> level_1 = new ArrayList<String>();
    ArrayList<String> level_2 = new ArrayList<String>();
    ArrayList<String> level_3 = new ArrayList<String>();

    int amountperlevel = cat_name.size()/3; // Jumlah kandang tiap level

    System.out.println("Cage arrangement:\n"+
                       "location: indoor");
    
    for(int i: this.cat_length){
      if(i < 45) cage.add("A");
      else if(i > 60) cage.add("C");
      else cage.add("B"); 
    }
      
      for (int i=0;i<this.cat_name.size();i++){
        unsort.add(this.cat_name.get(i)+" ("+this.cat_length.get(i)+" - "+cage.get(i)+")");
      }

      if (this.cat_name.size() >=3){
      for (int i=0;i<unsort.size();i++){
          if (i < amountperlevel) level_1.add(unsort.get(i));
          else if (i >= amountperlevel && i < (amountperlevel*2)) level_2.add(unsort.get(i));
          else level_3.add(unsort.get(i));      
      }
    }
      else if (this.cat_name.size() == 2){
        level_1.add(unsort.get(0));
        level_2.add(unsort.get(1));
        level_3.add("");
      }
      else{
        level_1.add(unsort.get(0));
        level_2.add("");
        level_3.add("");
      }

    System.out.println("level 3: " + String.join(", ", level_3));
    System.out.println("level 2: " + String.join(", ", level_2));
    System.out.println("level 1: " + String.join(", ", level_1));
    System.out.println();
    
    System.out.println("After rearrangement...");

    Collections.reverse(level_1);
    Collections.reverse(level_2);
    Collections.reverse(level_3);

    System.out.println("level 3: " + String.join(", ", level_2));
    System.out.println("level 2: " + String.join(", ", level_1));
    System.out.println("level 1: " + String.join(", ", level_3));
    System.out.println();
  }

  public void hamsterArrange(){

    ArrayList<String> cage = new ArrayList<String>();
    ArrayList<String> unsort = new ArrayList<String>();

    ArrayList<String> level_1 = new ArrayList<String>();
    ArrayList<String> level_2 = new ArrayList<String>();
    ArrayList<String> level_3 = new ArrayList<String>();

    int amountperlevel = hamster_name.size()/3; // Jumlah kandang tiap level

    System.out.println("Cage arrangement:\n"+
                       "location: indoor");
    
    for(int i: this.hamster_length){
      if(i < 45) cage.add("A");
      else if(i > 60) cage.add("C");
      else cage.add("B"); 
    }
      
    for (int i=0;i<this.hamster_name.size();i++){
      unsort.add(this.hamster_name.get(i)+" ("+this.hamster_length.get(i)+" - "+cage.get(i)+")");
    }

    if (this.hamster_name.size()>= 3){
      for (int i=0;i<unsort.size();i++){
          if (i < amountperlevel) level_1.add(unsort.get(i));
          else if (i >= amountperlevel && i < (amountperlevel*2)) level_2.add(unsort.get(i));
          else level_3.add(unsort.get(i));      
      }
  }
  
    else if (this.hamster_name.size() == 2){
      level_1.add(unsort.get(0));
      level_2.add(unsort.get(1));
      level_3.add("");
    }
    else{
      level_1.add(unsort.get(0));
      level_2.add("");
      level_3.add("");
    }

    System.out.println("level 3: " + String.join(", ", level_3));
    System.out.println("level 2: " + String.join(", ", level_2));
    System.out.println("level 1: " + String.join(", ", level_1));
    System.out.println();
    
    System.out.println("After rearrangement...");

    Collections.reverse(level_1);
    Collections.reverse(level_2);
    Collections.reverse(level_3);

    System.out.println("level 3: " + String.join(", ", level_2));
    System.out.println("level 2: " + String.join(", ", level_1));
    System.out.println("level 1: " + String.join(", ", level_3));
    System.out.println();
  }

  public void parrotArrange(){

    ArrayList<String> cage = new ArrayList<String>();
    ArrayList<String> unsort = new ArrayList<String>();

    ArrayList<String> level_1 = new ArrayList<String>();
    ArrayList<String> level_2 = new ArrayList<String>();
    ArrayList<String> level_3 = new ArrayList<String>();

    int amountperlevel = parrot_name.size()/3; // Jumlah kandang tiap level

    System.out.println("Cage arrangement:\n"+
                       "location: indoor");
    
    for(int i: this.parrot_length){
      if(i < 45) cage.add("A");
      else if(i > 60) cage.add("C");
      else cage.add("B"); 
    }
      
    for (int i=0;i<this.parrot_name.size();i++){
      unsort.add(this.parrot_name.get(i)+" ("+this.hamster_length.get(i)+" - "+cage.get(i)+")");
    }

    if (this.parrot_name.size() >=3){
      for (int i=0;i<unsort.size();i++){
          if (i < amountperlevel) level_1.add(unsort.get(i));
          else if (i >= amountperlevel && i < (amountperlevel*2)) level_2.add(unsort.get(i));
          else level_3.add(unsort.get(i));      
      }
  }
    else if (this.parrot_name.size() == 2){
      level_1.add(unsort.get(0));
      level_2.add(unsort.get(1));
      level_3.add("");
    }
    else{
      level_1.add(unsort.get(0));
      level_2.add("");
      level_3.add("");
    }


    System.out.println("level 3: " + String.join(", ", level_3));
    System.out.println("level 2: " + String.join(", ", level_2));
    System.out.println("level 1: " + String.join(", ", level_1));
    System.out.println();
    
    System.out.println("After rearrangement...");

    Collections.reverse(level_1);
    Collections.reverse(level_2);
    Collections.reverse(level_3);

    System.out.println("level 3: " + String.join(", ", level_2));
    System.out.println("level 2: " + String.join(", ", level_1));
    System.out.println("level 1: " + String.join(", ", level_3));
    System.out.println();
  }
  
  public void eagleArrange(){

    ArrayList<String> cage = new ArrayList<String>();
    ArrayList<String> unsort = new ArrayList<String>();

    ArrayList<String> level_1 = new ArrayList<String>();
    ArrayList<String> level_2 = new ArrayList<String>();
    ArrayList<String> level_3 = new ArrayList<String>();

    int amountperlevel = eagle_name.size()/3; // Jumlah kandang tiap level

    System.out.println("Cage arrangement:\n"+
                       "location: outdoor");
    
    for(int i: this.eagle_length){
      if(i < 75) cage.add("A");
      else if(i > 90) cage.add("C");
      else cage.add("B"); 
    }
      
    for (int i=0;i<this.eagle_name.size();i++){
      unsort.add(this.eagle_name.get(i)+" ("+this.eagle_length.get(i)+" - "+cage.get(i)+")");
    }

    if (this.eagle_name.size() >= 3){
    for (int i=0;i<unsort.size();i++){
          if (i < amountperlevel) level_1.add(unsort.get(i));
          else if (i >= amountperlevel && i < (amountperlevel*2)) level_2.add(unsort.get(i));
          else level_3.add(unsort.get(i));      
      }
  }

    else if (this.eagle_name.size() == 2){
      level_1.add(unsort.get(0));
      level_2.add(unsort.get(1));
      level_3.add("");
    }
    else{
      level_1.add(unsort.get(0));
      level_2.add("");
      level_3.add("");
    }
    System.out.println("level 3: " + String.join(", ", level_3));
    System.out.println("level 2: " + String.join(", ", level_2));
    System.out.println("level 1: " + String.join(", ", level_1));
    System.out.println();
    
    System.out.println("After rearrangement...");

    Collections.reverse(level_1);
    Collections.reverse(level_2);
    Collections.reverse(level_3);

    System.out.println("level 3: " + String.join(", ", level_2));
    System.out.println("level 2: " + String.join(", ", level_1));
    System.out.println("level 1: " + String.join(", ", level_3));
    System.out.println();
  }

// ---------------------------------------------------------------------- //

  public void lionArrange(){

    ArrayList<String> cage = new ArrayList<String>();
    ArrayList<String> unsort = new ArrayList<String>();

    ArrayList<String> level_1 = new ArrayList<String>();
    ArrayList<String> level_2 = new ArrayList<String>();
    ArrayList<String> level_3 = new ArrayList<String>();

    int amountperlevel = lion_name.size()/3; // Jumlah kandang tiap level

    System.out.println("Cage arrangement:\n"+
                       "location: outdoor");
    
    for(int i: this.lion_length){
      if(i < 75) cage.add("A");
      else if(i > 90) cage.add("C");
      else cage.add("B"); 
    }
    
      for (int i=0;i<this.lion_name.size();i++){
        unsort.add(this.lion_name.get(i)+" ("+this.lion_length.get(i)+" - "+cage.get(i)+")");
      }

    if (this.lion_name.size() >= 3){
      for (int i=0;i<unsort.size();i++){
          if (i < amountperlevel) level_1.add(unsort.get(i));
          else if (i >= amountperlevel && i < (amountperlevel*2)) level_2.add(unsort.get(i));
          else level_3.add(unsort.get(i));      
      }
    }
    
    else if (this.lion_name.size() == 2){
      level_1.add(unsort.get(0));
      level_2.add(unsort.get(1));
      level_3.add("");
    }
    else{
      level_1.add(unsort.get(0));
      level_2.add("");
      level_3.add("");
    }
    System.out.println("level 3: " + String.join(", ", level_3));
    System.out.println("level 2: " + String.join(", ", level_2));
    System.out.println("level 1: " + String.join(", ", level_1));
    System.out.println();
    
    System.out.println("After rearrangement...");

    Collections.reverse(level_1);
    Collections.reverse(level_2);
    Collections.reverse(level_3);

    System.out.println("level 3: " + String.join(", ", level_2));
    System.out.println("level 2: " + String.join(", ", level_1));
    System.out.println("level 1: " + String.join(", ", level_3));
    System.out.println();
  }

  public void printInfo(int cat, int hamster, int parrot, int eagle, int lion){
    System.out.println("ANIMALS NUMBER:");
    System.out.println("cat:"+cat);
    System.out.println("lion:"+lion);
    System.out.println("parrot:"+parrot);
    System.out.println("eagle:"+eagle);
    System.out.println("hamster:"+hamster);
    System.out.println();
  }
}