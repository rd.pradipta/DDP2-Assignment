/**
 * @author Rd Pradipta Gitaya S
 * @version 1.0
*/

import java.util.*;

// Class contain Main method
public class JavariPark{
    
    public static void main(String[] args) {

        Scanner inp = new Scanner(System.in);

        System.out.println("Welcome to Javari Park!\n"+
                         "input the number of animals");

        System.out.print("cat: ");
        int cat_amount = inp.nextInt();
            if (cat_amount != 0){
                System.out.println("Provide the information of cat(s)");
                inp.nextLine();
                String cat_data = inp.nextLine();
                JavariPark.listAdder(cat_data,"cat");
                }

        System.out.print("lion: ");
        int lion_amount = inp.nextInt();
            if (lion_amount != 0){
                System.out.println("Provide the information of lion(s)");
                inp.nextLine();
                String lion_data = inp.nextLine();
                JavariPark.listAdder(lion_data,"lion");}

        System.out.print("eagle: ");
        int eagle_amount = inp.nextInt();
            if (eagle_amount != 0){
                System.out.println("Provide the information of eagle(s)");
                inp.nextLine();
                String eagle_data = inp.nextLine();
                JavariPark.listAdder(eagle_data,"eagle");}

        System.out.print("parrot: ");
        int parrot_amount = inp.nextInt();
            if (parrot_amount != 0){
                System.out.println("Provide the information of parrot(s)");
                inp.nextLine();
                String parrot_data = inp.nextLine();
                JavariPark.listAdder(parrot_data,"parrot");}

        System.out.print("hamster: ");
        int hamster_amount = inp.nextInt();
            if (hamster_amount != 0){
                System.out.println("Provide the information of hamster(s)");
                inp.nextLine();
                String hamster_data = inp.nextLine();
                JavariPark.listAdder(hamster_data,"hamster");}
        
                Cage park = new Cage(cat_names,hamster_names,parrot_names,eagle_names,lion_names,
                                     cat_lengths,hamster_lengths,parrot_lengths,eagle_lengths,lion_lengths);

        System.out.println("Animals has been successfully recorded!\n"+
                           "=============================================");

    // Arrangement Starts Here.... //

        if (cat_amount !=0) park.catArrange();
        if (lion_amount !=0) park.lionArrange();
        if (eagle_amount !=0) park.eagleArrange();
        if (parrot_amount !=0) park.parrotArrange();
        if (hamster_amount !=0) park.hamsterArrange();

        park.printInfo(cat_amount,hamster_amount,parrot_amount,eagle_amount,lion_amount);

    // Arrangement Ends Here.... //

        boolean status = true;
        while (status){

        System.out.println("Which animal you want to visit?\n"+
                           "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)");
        int find_animal = inp.nextInt();
        if (find_animal <= 5 & find_animal >= 1){
            if (find_animal == 1)
            {
                System.out.print("Mention the name of cat you want to visit: ");
                inp.nextLine();
                String animal_name = inp.nextLine();
            
                Cat cat = new Cat(cat_names);

            if (cat.findCat(animal_name) == true)
            {
                cat.catMenu(animal_name);
                int choice = inp.nextInt();
                if (choice == 1) cat.doBrush(animal_name); 
                else if (choice == 2) cat.doCuddle(animal_name);
                else doNothing();
            }
            else animalNotFoundException("cat");
            }

            // ------------------------------ //
            else if (find_animal == 2)
            {
                System.out.print("Mention the name of eagle you want to visit: ");
                inp.nextLine();
                String animal_name = inp.nextLine();
            
                Eagle eagle = new Eagle(eagle_names);

            if (eagle.findEagle(animal_name) == true)
            {
                eagle.eagleMenu(animal_name);
                int choice = inp.nextInt();
                if (choice == 1) eagle.doFly(animal_name); 
                else doNothing();
            }
            else animalNotFoundException("eagle");
            }

            // ------------------------------ //
            else if (find_animal == 3)
            {
                System.out.print("Mention the name of hamster you want to visit: ");
                inp.nextLine();
                String animal_name = inp.nextLine();
            
                Hamster hamster = new Hamster(hamster_names);

            if (hamster.findHamster(animal_name) == true)
            {
                hamster.hamsterMenu(animal_name);
                int choice = inp.nextInt();
                if (choice == 1) hamster.doGnawing(animal_name); 
                else if (choice == 2) hamster.doRun(animal_name); 
                else doNothing();
            }
            else animalNotFoundException("hamster");
            }

            // ------------------------------ //

            else if (find_animal == 4)
            {
                System.out.print("Mention the name of parrot you want to visit: ");
                inp.nextLine();
                String animal_name = inp.nextLine();
            
                Parrot parrot = new Parrot(parrot_names);

            if (parrot.findParrot(animal_name) == true)
            {
                parrot.parrotMenu(animal_name);
                int choice = inp.nextInt();
                if (choice == 1) parrot.doFly(animal_name); 
                else if (choice == 2){
                    String dialogue = inp.nextLine();
                    parrot.doConversation(animal_name, dialogue);
                }
                else parrot.doNothing(animal_name);
            }
            else animalNotFoundException("parrot");
            }

            // ------------------------------ //

            else if (find_animal == 5)
            {
                System.out.print("Mention the name of lion you want to visit: ");
                inp.nextLine();
                String animal_name = inp.nextLine();
            
                Lion lion = new Lion(lion_names);

            if (lion.findLion(animal_name) == true)
            {
                lion.lionMenu(animal_name);
                int choice = inp.nextInt();
                if (choice == 1) lion.doHunt(animal_name); 
                else if (choice == 2) lion.doBrush(animal_name);
                else if (choice == 3) lion.doDisturb(animal_name);
                else doNothing();
            }
            else animalNotFoundException("lion");
            }
        }
        else if (find_animal == 99){
            System.out.println("Thank you for using our database");
            status = false;
        }
        else System.out.println("Wrong number! Please try again!");
        }
    }

    public static void listAdder(String data,String species){
        data = data.replace("|",",");
        String[] cleanlist = data.split(",");
        
        for (int i=0;i<cleanlist.length;i++){
            if (i%2==0){
                if (species == "cat") cat_names.add(cleanlist[i]);
                else if (species == "hamster") hamster_names.add(cleanlist[i]);
                else if (species == "parrot") parrot_names.add(cleanlist[i]);
                else if (species == "eagle") eagle_names.add(cleanlist[i]);
                else lion_names.add(cleanlist[i]);
            }
            else{
                if (species == "cat") cat_lengths.add(Integer.parseInt(cleanlist[i]));
                else if (species == "hamster") hamster_lengths.add(Integer.parseInt(cleanlist[i]));
                else if (species == "parrot") parrot_lengths.add(Integer.parseInt(cleanlist[i]));
                else if (species == "eagle") eagle_lengths.add(Integer.parseInt(cleanlist[i]));
                else lion_lengths.add(Integer.parseInt(cleanlist[i]));
            }
        }
    }
    public static void animalNotFoundException(String species){
        System.out.println("There is no " + species + " with that name! Back to the office!");
    }
    public static void doNothing(){
        System.out.println("You do nothing...");
        System.out.println("Back to the office!");
        System.out.println();
    }

    // Below are stored data as List for later use //

    // TAME ANIMAL NAMES //

    public static ArrayList<String> cat_names = new ArrayList<String>();
    public static ArrayList<String> hamster_names = new ArrayList<String>();
    public static ArrayList<String> parrot_names = new ArrayList<String>();

    // TAME ANIMAL LENGTHS //

    public static ArrayList<Integer> cat_lengths = new ArrayList<Integer>();
    public static ArrayList<Integer> hamster_lengths = new ArrayList<Integer>();
    public static ArrayList<Integer> parrot_lengths = new ArrayList<Integer>();

    // WILD ANIMAL NAMES //

    public static ArrayList<String> eagle_names = new ArrayList<String>();
    public static ArrayList<String> lion_names = new ArrayList<String>();

    // WILD ANIMAL LENGTHS //

    public static ArrayList<Integer> eagle_lengths = new ArrayList<Integer>();
    public static ArrayList<Integer> lion_lengths = new ArrayList<Integer>();    
}
