import java.util.*;

public class Cat{

  private ArrayList<String> cat_name;

  public Cat(ArrayList<String> cat_name){
    this.cat_name = cat_name;
  }

  public boolean findCat(String cat_name){
    int counter = 0;
    for (String i : this.cat_name){
      if (i.equals(cat_name)) counter+=1;
      else{
        continue;
      }
    }
    if (counter == 0) return false;
    else{
      return true;
    }
  }

  public void catMenu(String cat_name){
    System.out.println("You are visiting "+ cat_name + " (cat) now, what would you like to do?\n"+
    "1: Brush the fur 2: Cuddle");
  }

  public void doBrush(String cat_name){
    System.out.println("Time to clean "+cat_name+"\'s"+" fur");
    System.out.println(cat_name + " makes a voice: Nyaaan...\n"+
                      "Back to the office!");
  }

  public void doCuddle(String cat_name){
    String[] sounds = {"Miaaaw..","Purrr..","Mwaw!","Mraaawr!"};

    Random random = new Random();
    int idx = random.nextInt(sounds.length);

    System.out.println(cat_name + " makes a voice: " + sounds[idx]); // Ditambahin random ambil dari sounds
    System.out.println("Back to the office!");
  }
}