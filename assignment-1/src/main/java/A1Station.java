import java.util.Scanner;
public class A1Station {

    public static TrainCar kereta = null;
    private static final double THRESHOLD = 250; // in kilograms
    public static double catIn = 0;
    public static double catRem = 0;

    public static void main(String[] args) {

        Scanner inp = new Scanner(System.in);
        String firstinp = inp.nextLine();
        catRem = Integer.parseInt(firstinp); // Konversi string input pertama ke integer untuk digunkan kemudian

        while(true){
            for(int i =0;i < catRem; i++){
            String[] arrayinput = inp.nextLine().split(",");
            String catName = arrayinput[0];                       // Nama kucing
            double catWeight = Double.parseDouble(arrayinput[1]); // Nilai berat
            double catLength = Double.parseDouble(arrayinput[2]); // Nilai panjang

            WildCat cat = new WildCat(catName,catWeight,catLength); // Inisiasi
        
            if (kereta == null){
                kereta = new TrainCar(cat);
            }
            else{
                kereta = new TrainCar(cat,kereta);
            }
            
            catIn += 1; // Menghitung jumlah kucing yang sudah masuk
            
            if (kereta.computeTotalWeight() > THRESHOLD) break; // Break loop jika melebihi threshold
            
            }

            double bmi_raw = (kereta.computeTotalMassIndex()/catIn);
            String status_bmi;
    
            String bmi_raw2 = String.format("%.2f", bmi_raw);
            double bmi_round = Double.parseDouble(bmi_raw2);
    
            if (bmi_round < 18.5) status_bmi = "underweight";
            
            else if (18.5 <= bmi_round && bmi_round < 25) status_bmi = "normal";
            
            else if (25 <= bmi_round && bmi_round < 30) status_bmi = "overweight";
            
            else status_bmi = "obese";

            // Bagian untuk print

            System.out.println("The train departs to Javari Park");
            System.out.print("[LOCO]<--");
            kereta.printCar();
            System.out.println("Average mass index of all cats: " + bmi_raw2);
            System.out.println("In average, the cats in the train are "+status_bmi);

            // Mengulang memasukkan kucing dari awal pada rangkaian kereta baru
            
            catRem -= catIn;
            kereta = null;
            catIn = 0;

            if (catRem==0) break;
        }
    }
}
