    public class TrainCar {
   
    public static final double EMPTY_WEIGHT = 20; // In kilograms

    WildCat cat;
    TrainCar next;

    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    } 

    public double computeTotalWeight() {
        if (this.next == null){ // Ini untuk gerbong kereta di paling terakhir
            return (this.cat.weight + EMPTY_WEIGHT);
        }
        else{ // Ini kalau misalnya ada gerbong lain di belakang, sehingga menghitung total dari berat kereta
            return (EMPTY_WEIGHT + this.cat.weight) + this.next.computeTotalWeight();
        }
    }

    public double computeTotalMassIndex() {
        if(this.next == null){ // Ini kalau tidak ada gerbong kereta di belakang
            return this.cat.computeMassIndex();
        }
        else{ // Ini kalau ada gerbong kereta lain
            return (this.cat.computeMassIndex() + this.next.computeTotalMassIndex());
        }
    }

    public void printCar() { // Untuk printCar

        if(this.next == null){
            System.out.println("(" + this.cat.name + ")");
        }
        else{
            System.out.print("(" + this.cat.name + ")" + "--");
            this.next.printCar();
        }
    }
}