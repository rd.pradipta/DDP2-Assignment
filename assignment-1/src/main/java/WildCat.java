public class WildCat {

  String name; // Just to save the cat name
  double weight; // In kilograms
  double length; // In centimeters
  
  // Method menerima data
  
  public WildCat(String name, double weight, double length) { // Method menerima data
    this.name = name; // Deklarasi
    this.weight = weight; // Deklarasi
    this.length = length; // Deklarasi
  }
  // Method menghitung total BMI
    
  public double computeMassIndex() { // Menghitung total Massa Index
    return this.weight / ((this.length / 100.0) * (this.length / 100)); // Menghitung
  }
}
