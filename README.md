# Repository Kumpulan Submisi Assignment (Tugas Pemrograman) DDP 2
[![License Status](https://badges.frapsoft.com/os/mit/mit.svg?v=102)]
(https://gitlab.com/rd.pradipta/open-source-badge/)
[![Build Status](https://travis-ci.org/freeCodeCamp/how-to-contribute-to-open-source.svg?branch=master)](https://travis-ci.org/freeCodeCamp/how-to-contribute-to-open-source)

Dasar-dasar Pemrograman 2 - CSGE601021 | Fakultas Ilmu Komputer, Universitas Indonesia, Semester Genap 2017/2018

* Nama    : Rd Pradipta Gitaya S
* NPM     : 1706043361
* Kelas   : DDP 2 Kelas E
* Email   : rd.pradipta@ui.ac.id
* Jangan lupa, kunjungi juga GitHub saya di: [Link GitHub](https://github.com/diptags)

Tambahan: Saya sedang iseng buat LINE bot, dan saya butuh panduan dari para master, mungkin para master bisa memberikan saya saran :) Hehehe, link source codenya ada di link GitHub

## Daftar Isi

Repository ini akan berisi submisi dari soal Lab (Tutorial) DDP 02


1. [Tugas Pemrograman 1](https://gitlab.com/rd.pradipta/DDP2-Assignment/tree/master/assignment-1) - Tugas 1

2. [Tugas Pemrograman 2](https://gitlab.com/rd.pradipta/DDP2-Assignment/tree/master/assignment-2) - Tugas 2

3. [Tugas Pemrograman 3](https://gitlab.com/rd.pradipta/DDP2-Assignment/tree/master/assignment-3) - Tugas 3

4. [Tugas Pemrograman 4](https://gitlab.com/rd.pradipta/DDP2-Assignment/tree/master/assignment-4) - Tugas 4