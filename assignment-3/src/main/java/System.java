// Import required files

import javari.A3Festival;
import org.json.JSONWriter;

/* To make compile easier, this file is only supposed to run actual file in A3Festival.java
 * To run, just compile and run this file, let the process begins
 */

public class System {
    public static void main(String[] args) {
        A3Festival.run();
    }
}