package javari.park;

import java.util.*;

/**
 * This file contains interfaces that will be used later in the program
 * @author Rd Pradipta Gitaya S
 */
public interface Registration {

    // Return unique ID's for registered data
    int getRegistrationId();

    // Return names of visitors
    String getVisitorName();

    // Set visitor name for later JSON file
    String setVisitorName(String name);

    // Return attraction lists that will be availabe later
    List<SelectedAttraction> getSelectedAttractions();

    // Add new attraction that will be played by animals
    boolean addSelectedAttraction(SelectedAttraction selected);
}
