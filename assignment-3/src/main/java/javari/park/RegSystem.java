// Import required files

package javari.park;
import java.util.*;
import javari.park.*;

/**
 * This class will implement methods availabe in Registration.java
 * @author Rd Pradipta Gitaya S
 */

public class RegSystem implements Registration {

    // Instance Variables
    private int id;
    private String name;
    private List<SelectedAttraction> attractions;

    public RegSystem(int id, String name) {
        this.id = id;
        this.name = name;
        attractions = new ArrayList<SelectedAttraction>();
    }

    // Return unique ID's for registered data
    public int getRegistrationId() {
        return this.id;
    }

    // Return names of visitors
    public String getVisitorName() {
        return this.name;
    }

    // Set visitor name for later JSON file
    public String setVisitorName(String name) {
        this.name = name;
        return name;
    }

    // Return attraction lists that will be availabe later
    public List<SelectedAttraction> getSelectedAttractions() {
        return attractions;
    }

    // Add new attraction that will be played by animals
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        attractions.add(selected);
        return true;
    }
}
