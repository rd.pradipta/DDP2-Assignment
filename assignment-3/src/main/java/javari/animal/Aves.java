// Import required files
package javari.animal;

/**
 * This class provided certain data for Reptiles sections
 * @author Rd Pradipta Gitaya S
 */

public class Aves extends Animal {

    private String specificCondition;

    // Constructor to get Aves data

    public Aves(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, String specificCondition) {
        super(id, type, name, gender, length, weight, condition);
        this.specificCondition = specificCondition;
    }

    // Get specific condition to determine can show or not (Laying eggs)
    protected boolean specificCondition() {
        return !(specificCondition.equalsIgnoreCase("Laying eggs"));
    }
}
