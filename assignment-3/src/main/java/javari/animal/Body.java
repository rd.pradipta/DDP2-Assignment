// Import required files

package javari.animal;

/**
 * @author Rd Pradipta Gitaya S
 */
public class Body {

    // Instance variables

    private double length;
    private double weight;
    private Gender gender;

    // Constructor

    public Body(double length, double weight, Gender gender) {
        this.length = length;
        this.weight = weight;
        this.gender = gender;
    }

    // Return animal's length

    public double getLength() {
        return length;
    }

    // Return animal's weight

    public double getWeight() {
        return weight;
    }

    // Return animal's gender

    public Gender getGender() {
        return gender;
    }
}