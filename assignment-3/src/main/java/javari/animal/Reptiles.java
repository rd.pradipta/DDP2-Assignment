// Import required files
package javari.animal;

/**
 * This class provided certain data for Reptiles sections
 * @author Rd Pradipta Gitaya S
 */

public class Reptiles extends Animal {

    private String specificCondition;

    // Constructor
    public Reptiles(Integer id, String type, String name, Gender gender, double length,
                    double weight, Condition condition, String specificCondition) {
        super(id, type, name, gender, length, weight, condition);
        this.specificCondition = specificCondition;
    }

    // Get specific condition to determine can show or not (Tame)
    protected boolean specificCondition(){
        return (specificCondition.equalsIgnoreCase("Tame"));
    }
}
