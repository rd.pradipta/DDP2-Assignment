// Import required files
package javari.animal;

/**
 * This class provided what for Lion's specific data
 * @author Rd Pradipta Gitaya S
 */

public class Lion extends Mammals {

    // Constructor
    public Lion(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, String specificCondition) {
        super(id, type, name, gender, length, weight, condition, specificCondition);
    }

    // Get specific condition to determine can show or not (Female Lions)
    protected boolean specificCondition(){
        return super.specificCondition() && (this.getGender() == Gender.MALE);
    }
}
