// Import required files
package javari.animal;

/**
 * This class provided certain data for Mammals sections
 * @author Rd Pradipta Gitaya S
 */

public class Mammals extends Animal {

    private String specificCondition;

    // Constructor
    public Mammals(Integer id, String type, String name, Gender gender, double length,
                   double weight, Condition condition, String specificCondition) {
        super(id, type, name, gender, length, weight, condition);
        this.specificCondition = specificCondition;
    }

    // Get specific condition to determine can show or not (Pregnant)
    protected boolean specificCondition() {
        return !(specificCondition.equalsIgnoreCase("Pregnant"));
    }
}
