package javari.animal;

/**
 * This class represents common attributes and behaviours found in all animals
 * in Javari Park.
 * @author Rd Pradipta Gitaya S
*/
public abstract class Animal {

    // Some instance variables 

    private Integer id;
    private String type;
    private String name;
    private Body body;
    private Condition condition;

    /**
     * Constructs an instance of {@code Animal}.
     *
     * @param id        unique data for each visitor
     * @param type      type of animal
     * @param name      name of animal
     * @param gender    gender of animal
     * @param length    length of animal
     * @param weight    weight of animal (KG)
     * @param condition condition whether animal is healthy or not
     */

    // Constructor

    public Animal(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.body = new Body(length, weight, gender);
        this.condition = condition;
    }

    // Some getter for later use
    
    // Return unique ID's for registered data
    public Integer getId() {
        return id;
    }

    // Return type for animal (Mammals, Reptils, etc)
    public String getType() {
        return type;
    }

    // Return animal's name
    public String getName() {
        return name;
    }

    // Return animal's gender
    public Gender getGender() {
        return body.getGender();
    }

    // Return animal's length
    public double getLength() {
        return body.getLength();
    }

    // Return animal's weight
    public double getWeight() {
        return body.getWeight();
    }

    // Return animal's condition
    public Condition getCondition() {
        return condition;
    }

    // Return boolean to determine if animal can show OR not.
    public boolean isShowable() {
        return condition == Condition.HEALTHY && specificCondition();
    }

    // Get specific condition to determine can show or not (e.g laying eggs, female lion, etc)
    protected abstract boolean specificCondition();
}
