// Import required files

package javari.animal;

/**
 * This class describes possible genders for each animal.
 * @author Rd Pradipta Gitaya S
 */

// Don't really understand about this part, so I just leave this original

 public enum Gender {

    MALE, FEMALE;
    private static final String MALE_STR = "male";
    private static final String FEMALE_STR = "female";

    /**
     * Returns the correct gender enum based on given string representation
     * of a gender.
     *
     * @param str gender description
     * @return
     */
    public static Gender parseGender(String str) {
        if (str.equalsIgnoreCase(MALE_STR)) {
            return Gender.MALE;
        } else if (str.equalsIgnoreCase(FEMALE_STR)) {
            return Gender.FEMALE;
        } else {
            throw new UnsupportedOperationException();
        }
    }
}
