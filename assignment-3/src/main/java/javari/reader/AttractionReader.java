package javari.reader;

// Import required modules
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * This class to provide for reading all lines from CSV file.
 * @author Rd Pradipta Gitaya S
 */

 public class AttractionReader extends CsvReader {

    // Constructor
    public AttractionReader(Path file) throws IOException {
        super (file);
    }

    /**
     * Counts the number of valid records from CSV file
     */

    public long countValidRecords() {
        List<String> strList = this.getLines();
        String[] circlePlay = {"Whale", "Lion", "Eagle"};
        String[] dancePlay = {"Parrot", "Snake", "Cat", "Hamster"};
        String[] countingPlay = {"Parrot", "Whale", "Hamster"};
        String[] codersPlay = {"Snake", "Cat", "Hamster"};
        long cnt = 4;
        boolean cekCir, cekDan, cekCnt, cekCod;
        
        cekCir = true;
        cekDan = true;
        cekCnt = true;
        cekCod = true;
        
        for (String item : strList) {
            String[] atRec = item.split(",");
            if (atRec[1].equalsIgnoreCase("Circles of Fires")) {
                boolean tmp = false;
                for (int i = 0; i < 3; i++) {
                    if (atRec[0].equalsIgnoreCase(circlePlay[i])) tmp = true;
                }
                if (!tmp) cekCir = false;
            }
            if (atRec[1].equalsIgnoreCase("Dancing Animals")) {
                boolean tmp = false;
                for (int i = 0; i < 4; i++) {
                    if (atRec[0].equalsIgnoreCase(dancePlay[i])) tmp = true;
                }
                if (!tmp) cekDan = false;
            }
            if (atRec[1].equalsIgnoreCase("Counting Masters")) {
                boolean tmp = false;
                for (int i = 0; i < 3; i++) {
                    if (atRec[0].equalsIgnoreCase(countingPlay[i])) tmp = true;
                }
                if (!tmp) cekCnt = false;
            }
            if (atRec[1].equalsIgnoreCase("Passionate Coders")) {
                boolean tmp = false;
                for (int i = 0; i < 3; i++) {
                    if (atRec[0].equalsIgnoreCase(codersPlay[i])) tmp = true;
                }
                if (!tmp) cekCod = false;
            }
        }
        if (!cekCir) cnt--;
        if (!cekDan) cnt--;
        if (!cekCnt) cnt--;
        if (!cekCod) cnt--;
        return cnt;
    }

    /**
     * Counts the number of INVALID records from CSV file
     */

    public long countInvalidRecords() {
        long cnt = 4 - countValidRecords();
        List<String> strList = this.getLines();
        for (String item : strList) {
            String[] atRec = item.split(",");
            if (!atRec[1].equalsIgnoreCase("Circles of Fires")) {
                if (!atRec[1].equalsIgnoreCase("Dancing Animals")) {
                    if (!atRec[1].equalsIgnoreCase("Counting Masters")) {
                        if (!atRec[1].equalsIgnoreCase("Passionate Coders")) {
                            cnt++;
                        }
                    }
                }
            }
        }
        return cnt;
    }
}