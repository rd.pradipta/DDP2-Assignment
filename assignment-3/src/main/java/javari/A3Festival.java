package javari;

// Import required modules
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

// Import required files
import javari.animal.*;
import javari.park.*;
import javari.reader.*;
import javari.writer.*;

public class A3Festival {

    public static void run() {

        // Initialize Scanner for user input
        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("Please provide the source data path, or type data to start immediately : ");
        String inp = input.nextLine(); // Receive input from user
        
        // Copy path so CSV Reader can do the job
        Path oPath = Paths.get(inp);
        Path path1 = Paths.get(inp + "\\animals_categories.csv");
        Path path2 = Paths.get(inp + "\\animals_attractions.csv");
        Path path3 = Paths.get(inp + "\\animals_records.csv");

        Section firstSec = new ExploreTheMammals();
        Section secSec = new WorldOfAves();
        Section thirdSec = new ReptillianKingdom();
        RegSystem registration = new RegSystem(1, "");

        AttractionReader attReader;
        CategoriesReader catReader;
        RecordsReader recReader;
        ArrayList < Animal > animalAvailabe = new ArrayList < Animal > ();

        try {
            catReader = new CategoriesReader(path1);
            attReader = new AttractionReader(path2);
            recReader = new RecordsReader(path3);
            System.out.println("");
            System.out.println("... Loading... Success... System is populating data...\n");

            // Print whether CSV file already read
            System.out.format("Found %d valid sections and %d invalid sections\n",
                catReader.countValidSections(), catReader.countInvalidSections());
            System.out.format("Found %d valid attractions and %d invalid attractions\n",
                attReader.countValidRecords(), attReader.countInvalidRecords());
            System.out.format("Found %d valid animal categories and %d invalid animal categories\n",
                catReader.countValidRecords(), catReader.countInvalidRecords());
            System.out.format("Found %d valid animal records and %d invalid animal records\n\n",
                recReader.countValidRecords(), recReader.countInvalidRecords());

            animalAvailabe = recReader.getList();
        } catch (IOException e) { // In  case file not found or invalid path
            System.out.println("Cannot Read File or File Not Found !.. System Exiting ..\n");
            System.exit(0);
        }

        for (Animal current: animalAvailabe) {
            if (current.getType().equalsIgnoreCase("Snake")) {
                thirdSec.addAnimal(current);
            } else if (current.getType().equalsIgnoreCase("Eagle") ||
                current.getType().equalsIgnoreCase("Parrot")) {
                secSec.addAnimal(current);
            } else {
                firstSec.addAnimal(current);
            }
        }
        System.out.println("Welcome to Javari Park Festival - Registration Service!\n" + "Please answer the questions by typing the number. \n" + "Type # if you want to return to the previous menu\n");

        while (true) {
            System.out.println( "Javari park has 3 sections:\n" +
                                "1. Explore the Mammals\n" + 
                                "2. World of Aves\n" +
                                "3. Reptilian Kingdom\n" +
                                "Please choose your preferred section (type the number): ");
            inp = input.nextLine();
            Attraction selAtt;
            if (inp.equals("#")) break;
            int op = Integer.parseInt(inp);
            if (op == 1) {
                selAtt = firstSec.displayOptions();
            } else if (op == 2) {
                selAtt = secSec.displayOptions();
            } else {
                selAtt = thirdSec.displayOptions();
            }
            
            if (selAtt != null) {
                System.out.println("Wow, one more step,");
                System.out.print("please let us know your name :");
                String visitor = input.nextLine();

                System.out.println("");
                System.out.println("Yeay, final check!");
                System.out.println("Here is your data and the attraction you choose:");
                System.out.format("Name : %s\n", visitor);
                System.out.format("Attractions: %s -> %s\n", selAtt.getName(), selAtt.getType());
                System.out.print("With:");
         
                for (Animal item: selAtt.getPerformers()) {
                    System.out.format(" %s,", item.getName());
                }
                System.out.println("\n");
                System.out.print("Is the data correct? (Y/N): ");
                inp = input.nextLine();
         
                if (inp.equalsIgnoreCase("Y")) {
                    registration.setVisitorName(visitor);
                    registration.addSelectedAttraction(selAtt);
                    System.out.print("Thank you for your interest. " +
                        "Would you like to register to other attractions? (Y/N): ");
                    inp = input.nextLine();
                    if (inp.equalsIgnoreCase("N")) {
                        try {
                            RegWriter.writeJson(registration, oPath);
                        } catch (IOException e) {
                            System.out.println("There is an error ...");
                            System.exit(0);
                        }
                        System.exit(0);
                    }
                }
            }
        }

    }
}